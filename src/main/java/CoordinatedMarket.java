import java.time.LocalTime;

public class CoordinatedMarket {

    public static final Object Market = new Object();

    public static void coordinatedMarket() {

        if (Thread.currentThread().getName().equals("Alice")) {

            Share share1 = new Share("Alice");
            share1.equalPrice();

            try {
                synchronized (Market) {
                    Market.wait(5000);
                    System.out.println(LocalTime.now() + ": " + Thread.currentThread().getName() + " зареєстровано");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (Thread.currentThread().getName().equals("Bob")) {

            Share share1 = new Share("Bob");
            share1.equalPrice();

            try {
                synchronized (Market) {
                    Market.wait(5000);
                    System.out.println(LocalTime.now() + ": " + Thread.currentThread().getName() + " зареєстровано");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (Thread.currentThread().getName().equals("Charlie")) {
            Share share1 = new Share("Charlie");
            share1.equalPrice();

            try {
                synchronized (Market) {
                    Market.wait(5000);
                    System.out.println(LocalTime.now() + ": " + Thread.currentThread().getName() + " зареєстровано");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        System.out.println(LocalTime.now() + ": Початок виконання");

        new Thread(CoordinatedMarket::coordinatedMarket, "Alice").start();
        new Thread(CoordinatedMarket::coordinatedMarket, "Bob").start();
        new Thread(CoordinatedMarket::coordinatedMarket, "Charlie").start();


        for (int i = 0; i < 3; i++) {
            Share share1 = new Share("Market");
            share1.changeMarket();
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized (Market) {
            Market.notify();
        }
    }
}








