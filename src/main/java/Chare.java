class Share {
    public String name;
    public String shar;
    public Integer amount;
    public Integer price;

    public Share[] sharesMarket = {new Share("Market", "AAPL", 100, 141), new Share("Market", "COKE", 1000, 378), new Share("Market", "IBM", 200, 137)};
    public Share[] aliceShars = {new Share("Alice", "AAPL", 10, 100), new Share("Alice", "COKE", 20, 390)};
    public Share[] bobShars = {new Share("Bob", "Bob", 10, 140), new Share("Bob", "IBM", 20, 135)};
    public Share[] charliShars = {new Share("Charlie", "COKE", 300, 370)};

    Share(String name, String shar, Integer amount, Integer price) {
        this.amount = amount;
        this.price = price;
        this.shar = shar;
        this.name = name;
    }

    Share(String name) {
        this.name = name;
    }

    Integer getPrice(String name) {
        return price;
    }

    Integer getAmount() {
        return amount;
    }

    String getShar() {
        return shar;
    }

    void equalPrice() {
        if (name.equals("Alice")) {
            for (Share shares : sharesMarket) {
                for (Share sharesAlice : aliceShars) {
                    if (shares.equals(sharesAlice)) {
                        if (shares.price < sharesAlice.price)
                            System.out.println("Покупаем " + sharesAlice.shar + " в количестве " + Math.min(sharesAlice.amount, sharesAlice.amount) + " покупатель " + "Alice ");

                    } else {
                        System.out.println("НЕ Покупаем " + sharesAlice.shar + " в количестве " + Math.min(sharesAlice.amount, sharesAlice.amount) + " покупатель " + "Alice");
                    }
                }
            }
        }
        if (this.name.equals("Bob")) {
            for (Share shares : sharesMarket) {
                for (Share sharesBob : bobShars) {
                    if (shares.equals(sharesBob)) {
                        if (shares.price < sharesBob.price) {
                            System.out.println("Покупаем " + sharesBob.shar + " в количестве " + Math.min(sharesBob.amount, shares.amount) + " покупатель " + "Bob ");

                        } else {
                            System.out.println("НЕ Покупаем " + sharesBob.shar + " в количестве " + Math.min(sharesBob.amount, shares.amount) + " покупатель " + "Bob");
                        }
                    }
                }
            }
        }
        if (this.name.equals("Charlie")) {
            for (Share shares : sharesMarket) {
                for (Share sharesCharli : charliShars) {
                    if (shares.equals(sharesCharli)) {
                        if (shares.price < sharesCharli.price) {
                            System.out.println("Покупаем " + sharesCharli.shar + " в количестве " + Math.min(sharesCharli.amount, shares.amount) + " покупатель " + "Bob ");
                        }else {
                            System.out.println("НЕ Покупаем " + sharesCharli.shar + " в количестве " + Math.min(sharesCharli.amount, shares.amount) + " покупатель " + "Bob");
                        }
                    }
                }
            }
        }
    }

    void changeMarket(){
        for (Share marketShars: sharesMarket ){
           int i0 = (int) (Math.random() * 3);
           marketShars.price = marketShars.price *(1+i0/100);
        }
    }

}

